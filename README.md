# Telegram IRC client

An irc client that uses Telegram as an user interface that runs on node.js

## Setup

download this repository

`npm install`

rename `example_config.js` to `config.js` and edit it (file is commented)

`node index.js` to run

## Usage

this irc client uses the concept of an active channel.
All mesages and channel specific commands that you type will go to that active channel.
To make a channel active /join it, the currently active channel will be moved to the background. (/join will (as the name implies) automatically join a channel if its not in the background).
You will receive messages from both the active and the background channel,with the exception, that no channel name will be displayed for messaes from the active channel.
To open a private chat simply /join &lt;username>.
You will always receive private messages.
To leave a channel /part it.

To see you nickname,active channel and backgrounf channels type /info .

Other commands that behave like in other irc clients and are implemented:

- /kick &lt;user> &lt;message>
- /me &lt;message>
- /msg &lt;user/channel> &lt;message>
- /nick &lt;newnickname>
- /notice &lt;user/channel> &lt;message>
- /whois &lt;user>

## TODO

- add support for the /list command
- add a userlist command, that lists all users in the current channel
- add support for mot operator commands
- fancy output for the whois command

## Contributing
If you really want to ...

fork > commit > pull request > Hope that I see it ...