var irc = require('irc');
var TelegramBot = require("node-telegram-bot-api");
var config = require("./config")

var active_irc_channel=""
var irc_nick = config.irc.nick

//initalize bots
var bot = new TelegramBot(config.telegram.token, {polling:true});
console.log("connecting to irc server "+config.irc.server+" as "+config.irc.nick);
bot.sendMessage(config.telegram.ownerid,"connecting to irc server "+config.irc.server+" as "+config.irc.nick);
var client = new irc.Client(config.irc.server, config.irc.nick, {
    channels: config.irc.channels,
    realName: config.irc.realName,
    userName: config.irc.nick,
  //  secure: true,
  //  autoConnect: false,
});

console.log("bots initalized !");

/*
client.connect(10,() =>{
  console.log("connected")
});
*/

//helper functions

function telegramCanUse(userId){
  if(userId!=config.telegram.ownerid){
    bot.sendMessage(userId,"This is a private bot!\nGo home!");
    return false;
  }
  return true;
}

function listToString(array,seperator) {
  out = "";
  for(i=0;i<array.length;i++){
    if(i>0){out=out+seperator;}
    out=out+array[i]
  }
  return out;
}

function escapeMarkdown(text){
  return text.replace( new RegExp("_","gm"),"\\_").replace( new RegExp("[*]","gm"),"\\*").replace( new RegExp("`","gm"),"\\`").replace( new RegExp("\\[","gm"),"\\[")
}

function escapeHTML(text){
 return text.replace( new RegExp("&","gm"),"&amp;").replace( new RegExp("<","gm"),"&lt;").replace( new RegExp(">","gm"),"&gt;")
}

function listJoinedChannels(){
  return Object.keys(client.chans);
}

function isPrivate(channel){
  return !channel.startsWith("#")
}

function goChannel(channel){
  if(isPrivate(channel)){
    bot.sendMessage(config.telegram.ownerid,"Active channel: "+channel+" [private]");
    active_irc_channel = channel;
    return;
  }
  if(listJoinedChannels().indexOf(channel)==-1){
    client.join(channel,function(){
      bot.sendMessage(config.telegram.ownerid,"Joined channel "+channel);
      active_irc_channel = channel;
    });
  }else{
    bot.sendMessage(config.telegram.ownerid,"Active channel: "+channel);
    active_irc_channel = channel;  
  }
}

function leaveChannel(channel,message){
  if(isPrivate(channel)){
    active_irc_channel = "";
    bot.sendMessage(config.telegram.ownerid,"No active channel: /join another one ...");
    return;
  }
  if(message == null | message === undefined){
    message = config.irc.defaultPartMessage;
  }
  if(listJoinedChannels().indexOf(channel)!=-1){
    if(channel == active_irc_channel){
      active_irc_channel = "";
      bot.sendMessage(config.telegram.ownerid,"No active channel: /join another one ...");
    }
    client.part(channel,message,function(){
      bot.sendMessage(config.telegram.ownerid,"Left "+channel);
    })
  }
}

//irc clent listeners

client.addListener("registered", function(message) {
  console.log("connected to irc!");
  bot.sendMessage(config.telegram.ownerid,"Connected to IRC!\n-----------------------------------------");
  if(config.irc.onConnect !== undefined){
     setTimeout(function(){
        if(config.irc.channels.length > 0){
          goChannel(config.irc.channels[0])
        }
        config.irc.onConnect(client);
     },1000);
  }
  //client.say('##bottest', 'hello!');
});

client.addListener('error', function(message) {
    console.log('ircerror: ', message);
});

client.addListener('message', function (from, to, message) {
    //console.log(from + ' => ' + to + ': ' + message);
    if(to == active_irc_channel){
      bot.sendMessage(config.telegram.ownerid,from+": "+message)
    }else if(to == config.irc.nick){
      bot.sendMessage(config.telegram.ownerid,">"+from+"<: "+message)
    }else{
      bot.sendMessage(config.telegram.ownerid,from+">"+to+": "+message)    
    }
});

client.addListener('action', function (from, to, text, message) {
    from = escapeHTML(from)
    text = escapeHTML(text)
    if(to == active_irc_channel){
      bot.sendMessage(config.telegram.ownerid,"<i>"+from+" "+text+"</i>",{parse_mode:"HTML"})
    }else if(to == config.irc.nick){
      bot.sendMessage(">"+config.telegram.ownerid,"<i>&gt"+from+"&lt "+text+"</i>",{parse_mode:"HTML"})
    }else{
      bot.sendMessage(config.telegram.ownerid,"<i>"+from+"&gt"+escapeHTML(to)+" "+text+"</i>",{parse_mode:"HTML"})    
    }
});

client.addListener("nick", function(oldnick, newnick, channels, message){
  oldnick = escapeHTML(oldnick);
  newnick = escapeHTML(newnick);
  bot.sendMessage(config.telegram.ownerid,"<b>"+oldnick+" is now known as "+newnick+"</b>",{parse_mode:"HTML"});
});

//bot.sendMessage(config.telegram.ownerid,"");

client.addListener("join", function (channel, nick, message) {
  if(channel==active_irc_channel){
    bot.sendMessage(config.telegram.ownerid,nick+" has joined");
  }else{
    bot.sendMessage(config.telegram.ownerid,nick+" has joined "+channel);
  }
});

client.addListener("part", function (channel, nick, reason, message) {
  if(channel==active_irc_channel){
    bot.sendMessage(config.telegram.ownerid,nick+" left ("+reason+")");
  }else{
    bot.sendMessage(config.telegram.ownerid,nick+" left "+channel+" ("+reason+")");
  }
});

client.addListener("quit", function (nick, reason, channels, message) {
  bot.sendMessage(config.telegram.ownerid,nick+" has quit ("+reason+")");
});

client.addListener("kick", function (channel, nick, by, reason, message) {
  if(channel==active_irc_channel){
    bot.sendMessage(config.telegram.ownerid,nick+" has been kicked by "+by+" ("+reason+")");
  }else{
    bot.sendMessage(config.telegram.ownerid,nick+" has been kicked from "+channel+" by "+by+" ("+reason+")");
  }
});

client.addListener("kill", function (nick, reason, channels, message)  {
  bot.sendMessage(config.telegram.ownerid,nick+" has been killed from the server ("+reason+")");
});

client.addListener("notice", function (nick, to, text, message) {
  if(nick == null || nick === undefined){
    if(to=="*"){
      bot.sendMessage(config.telegram.ownerid,"NOTICE:"+text);
    }else{
      bot.sendMessage(config.telegram.ownerid,"NOTICE => "+to+"\n"+text);
    }
  }else{
    bot.sendMessage(config.telegram.ownerid,"NOTICE "+nick+" => "+to+"\n"+text);
  }
});

client.addListener("+mode", function (channel, by, mode, argument, message) {
  if(by === undefined){by = config.irc.server;}
  if(argument === undefined){
      bot.sendMessage(config.telegram.ownerid,by+" sets mode +"+mode+" on "+channel);
  }else{
    bot.sendMessage(config.telegram.ownerid,by+" sets mode +"+mode+" on "+channel+" ("+argument+")");
  }
});

client.addListener("-mode", function (channel, by, mode, argument, message) {
  if(by === undefined){by = config.irc.server;}
  if(argument === undefined){
      bot.sendMessage(config.telegram.ownerid,by+" sets mode -"+mode+" on "+channel);
  }else{
    bot.sendMessage(config.telegram.ownerid,by+" sets mode -"+mode+" on "+channel+" ("+argument+")");
  }
});

client.addListener("topic", function (channel, topic, nick, message) {
  if(channel==active_irc_channel){
    bot.sendMessage(config.telegram.ownerid,"TOPIC:"+topic);
  }else{
    bot.sendMessage(config.telegram.ownerid,"TOPIC for channel "+channel+" is:\n"+topic);
  }
});

client.addListener("whois",function(info){
  //bot.sendMessage(config.telegram.ownerid,"["+info.nick+"]\n("+info.user+"@"+info.host+"):\n"+realname+"\n"+info.server+" : "+serverinfo+"\n"+listToString(info.channels," "))
  bot.sendMessage(config.telegram.ownerid,"Whois response:\n"+JSON.stringify(info))
});

//telegram bot listeners

//no command (message)
bot.onText(/^([^\/].*)/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  if(active_irc_channel == ""){
    bot.sendMessage(config.telegram.ownerid,"Currently there is no channel active!");
    return;
  }
  client.say(active_irc_channel,msg.text)
});

bot.onText(/^\/me (.*)/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  if(active_irc_channel == ""){
    bot.sendMessage(config.telegram.ownerid,"Currently there is no channel active!");
    return;
  }
  client.action(active_irc_channel,match[1])
});

bot.onText(/^\/kick ([^ ]*) (.*)/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  if(isPrivate(channel)){return;}
  if(active_irc_channel == ""){
    bot.sendMessage(config.telegram.ownerid,"Currently there is no channel active!");
    return;
  }
  client.send("KICK",match[1],match[2])
});

bot.onText(/^\/msg ([^ ]*) (.*)/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  client.say(match[1],match[2])
});

bot.onText(/^\/notice ([^ ]*) (.*)/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  client.notice(match[1],match[2])
});

bot.onText(/^\/whois ([^ ]*)/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  client.whois(match[1])
});

bot.onText(/^\/nick ([^ ]*)/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  client.send("NICK",match[1])
});

bot.onText(/^\/info/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  //console.log("active_channel: "+active_irc_channel+"\nnick:"+irc_nick+"\njoined channels:\n"+listToString(irc_channels," "))
  //console.log(Object.keys(client.chans));
  bot.sendMessage(msg.from.id,"active_channel: "+active_irc_channel+"\nnick:"+irc_nick+"\njoined channels:\n"+listToString(listJoinedChannels()," "))
});

bot.onText(/^\/join (.*)/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  channel = match[1];
  console.log("Joining channel "+channel)
  goChannel(channel);
});

bot.onText(/^\/part ([^ ]*) (.*)$/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  channel = match[1]
  leaveChannel(channel,match[2]);
});

bot.onText(/^\/part ([^ ]*)$/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  channel = match[1]
  leaveChannel(channel);
});

bot.onText(/^\/part$/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  channel = active_irc_channel;
  leaveChannel(channel);
});

bot.onText(/^\/reconnect/,function(msg,match){
  if(!telegramCanUse(msg.from.id)){return;}
  channels = listJoinedChannels();
  client.disconnect("reconnecting ...",function(){
      client.connect(10,function(){
          setTimeout(function(){
              for(var i=0;i<channels.length;i++){
                  goChannel(channels[i])
                  goChannel(active_irc_channel)
              }
          },1500);
      });
  });
});


bot.on('polling_error', (error) => {
  console.log("Telegram error:"+error.code);  // => 'EFATAL'
  console.log(error)
});
