module.exports = new Object();

//irc
var irc = {}
irc.server="portlane.esper.net" //the irc server you want to connect to
irc.port=6667 //the port the server is listening on
irc.nick="someone" //put your irc nich here
irc.realName="Has it ever crossed your mind that names are not real?" //put you "real" name here (or not)
irc.channels=['##bottest'] //change this to the channels you want to join at startup
irc.defaultPartMessage="Bye" //The message that gets sent when you /part a channel

irc.onConnect=function(client){
  client.send("NICK",irc.nick); //resets the nockname on reconnect
  client.say("NickServ","identify <password>");//comment this out if you don't have an account
}

module.exports.irc = irc

//telegram
var tg={}
tg.token = "paste your bot token here" //Talk to @BotFather to get this token
tg.ownerid = 0 //replace this with your user id (@get_id_bot or similar ones will help you)


module.exports.telegram = tg

